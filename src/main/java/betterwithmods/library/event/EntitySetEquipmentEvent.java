package betterwithmods.library.event;

import net.minecraft.entity.Entity;
import net.minecraft.world.DifficultyInstance;
import net.minecraftforge.event.entity.EntityEvent;

public class EntitySetEquipmentEvent extends EntityEvent {

    private DifficultyInstance difficulty;

    public EntitySetEquipmentEvent(Entity entity, DifficultyInstance difficulty) {
        super(entity);
        this.difficulty = difficulty;
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}
